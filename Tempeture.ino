#include <math.h>
#define A1 0.003354016 //datasheet
#define B1 0.0002569850
#define C1 0.000002620131
#define D1 0.00000006383091

#define AN_OFFSET 280// "Calibration done with external sensor"
#define AN_OFFSET_BODY 170

//Datasheet:http://cdn.sparkfun.com/datasheets/Sensors/Temp/ntcle100.
float vcc = 4.8; //voltage of the sensor
float Fix_resistor=10000; // resistance serie
float therm_nominal_resistance = 10000; //nominal peltier

float Resistance;
float Temp;



float calculTemp(int i){
    float anValue=analogRead(peltier_pins[i][3])-AN_OFFSET;
    float va=(vcc/1024)*anValue;
    float intensity=va/Fix_resistor;

    Resistance=((5-va)/intensity);
    
    float t= ((1024/anValue)-1)*10000;
    
    float aux=0;
    aux=log(Resistance/therm_nominal_resistance);
    float Kalvin=0;
    Kalvin=(1/(A1+B1*aux+C1*aux*aux+D1*aux*aux*aux));
    float temp=Kalvin-273.15;
   // Serial.println(temp);
   return temp;
 
}


float calculTempBody(){
  //BODY
 // analogReadResolution(10);
    float anValue=analogRead(tempBodySensor)-AN_OFFSET_BODY;
    float va=(vcc/1024)*anValue;
    float intensity=va/Fix_resistor;

  Resistance=((5-va)/intensity);
    
    
    float t= ((1024/anValue)-1)*10000;
    
    float aux=0;
    //aux=log(Resistance/therm_nominal_resistance);
    aux=log(Resistance/6800);
    float Kalvin=0;
    Kalvin=(1/(A1+B1*aux+C1*aux*aux+D1*aux*aux*aux));
    float temp=Kalvin-273.15;
   // Serial.println(temp);
   return temp;
 
}





