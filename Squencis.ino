#define timeSeq 3000 //default value for each action



String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete
int valueAux=0;

        
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      stringComplete = true;
      Serial.println(inputString);
    }
  }
}



void printMenu1(){
    Serial.print("&");
  Serial.println("1-Asynchronous");
  Serial.println("2- Sequential/ synchronous");
  Serial.println("99-->SPAM");
  Serial.println("-1-->OFF");
  Serial.println("-2-->Ambient");
    Serial.print("%");
}

void printAsyncMenu(){
    Serial.print("&");
Serial.println("I. For asynchronous (rate of change being 3C/sec over 2 sec):");
Serial.println("");
Serial.println("99-->SPAM");
  Serial.println("-1-->OFF");
  Serial.println("-2-->Ambient");
Serial.println("0-->Exit");
Serial.println("Condition 1 : cold (1) cold (2)");
Serial.println("Condition 2: cold (1) hot (2)");
Serial.println("Condition 3: hot (1) cold (2)");
Serial.println("Condition 4: hot (1) hot (2)");
Serial.println("Condition 5: cold (2) cold (1)");
Serial.println("Condition 6: cold (2) hot (1)");
Serial.println("Condition 7: hot (2) cold (1)");
Serial.println("Condition 8: hot(2) hot (1)");
  Serial.print("%");
}


void printSyncMenu(){
  Serial.print("&");
Serial.println("II. Sequential/ synchronous (rate of change being 3C/sec over 2 sec):  This is straightforward - the direction of activation is constant throughout."); 
Serial.println("");
Serial.println("0-->Exit");
  Serial.println("-1-->OFF");
  Serial.println("-2-->Ambient");
Serial.println("99-->SPAM");
Serial.println("Condition 1: cold(1) -- cold(2) ");
Serial.println("Condition 2: cold(1) -- hot(2)");
Serial.println("Condition 3: hot(1) -- cold(2)");
Serial.println("Condition 4: hot(1) -- hot(2)");
  Serial.print("%");
}

int M_Motor=0;
void menu(){
  switch (M_Motor){
    case 0:
      printMenu1();
      M_Motor=1;
    break;
    case 1:
          if (stringComplete==true){
          valueAux=(inputString.toInt());
          // clear the string:
          inputString = "";
          stringComplete = false;
          switch (valueAux){
            case 1:
              printAsyncMenu();
              M_Motor=2;
              break;
              case 2:
              printSyncMenu();
              M_Motor=3;
              break;
             case 99:
               if (spam){spam=false;}else {spam=true;}
               M_Motor=0;
               break;
              
             case -1:
               turnOffAllPeltier();
               Ambient=false;
               M_Motor=0;
               break;

              case -2:
                if (Ambient){Ambient=false;}else{Ambient=true;}
               break;
    
              default:
              Serial.println("not a valid option");
              M_Motor=0;
              break; 
             
          }
          }
       break;
       
       case 2:
            if (stringComplete==true){
          valueAux=(inputString.toInt());
          // clear the string:
          inputString = "";
          stringComplete = false;
          if (valueAux==0){M_Motor=0;}else{
           AsyncAction(valueAux);
           printAsyncMenu();
           }
          }
       break;
       case 3:
         if (stringComplete==true){   
          valueAux=(inputString.toInt());
          // clear the string:
          inputString = "";
          stringComplete = false;
          if (valueAux==0){M_Motor=0;}else{
          SyncAction(valueAux);
          printSyncMenu();
          }
          }
          break;
      }
}

void AsyncAction(int valueAux){

 Serial.print("^");
  switch(valueAux){
    
    case 1:
    Serial.println("Condition 1 : cold (1) cold (2)");
    Serial.print("%");
      addWork(0,COLDTEMP,timeSeq);
      addWork(1,COLDTEMP,timeSeq);
    break;
    case 2:
    Serial.println("Condition 2: cold (1) hot (2)");
    Serial.print("%");
      addWork(0,COLDTEMP,timeSeq);
      addWork(1,HOTTEMP,timeSeq);
    break;
    case 3:
    Serial.println("Condition 3: hot (1) cold (2)");
    Serial.print("%");
     addWork(0,HOTTEMP,timeSeq);
     addWork(1,COLDTEMP,timeSeq);
    break;
    case 4:
    Serial.println("Condition 4: hot (1) hot (2)");
    Serial.print("%");
    addWork(0,HOTTEMP,timeSeq);
    addWork(1,HOTTEMP,timeSeq);
    break;
    case 5: 
    Serial.println("Condition 5: cold (2) cold (1)");
    Serial.print("%");
    addWork(1,COLDTEMP,timeSeq);
    addWork(0,COLDTEMP,timeSeq);
    break;
        case 6:
    Serial.println("Condition 6: cold (2) hot (1)");
    Serial.print("%");
      addWork(1,COLDTEMP,timeSeq);
      addWork(0,HOTTEMP,timeSeq);
    break;
    case 7:
    Serial.println("Condition 7: hot (2) cold (1)");
    Serial.print("%");
     addWork(1,HOTTEMP,timeSeq);
     addWork(0,COLDTEMP,timeSeq);
    break;
    case 8:
    Serial.println("Condition 8: hot (2) hot (1)");
    Serial.print("%");
    addWork(1,HOTTEMP,timeSeq);
    addWork(0,HOTTEMP,timeSeq);
    break;
    case 99:
      if (spam){spam=false;}else {spam=true;}
     break;
    default:
    Serial.println("Wrong Choice");
    Serial.print("%");
    break;
  }
  Serial.println("%");
  Serial.print("%");
}


void SyncAction(int valueAux){
  Serial.print("^");
    switch(valueAux){
      case 1:
      Serial.println("Condition 1: cold(1) -- cold(2) ");
      Serial.print("%");
      setTemperature(0, COLDTEMP, timeSeq);
      setTemperature(1, COLDTEMP, timeSeq);
    break;
    case 2:
 Serial.println("Condition 2: cold(1) -- hot(2)");
 Serial.print("%");
       setTemperature(0, COLDTEMP, timeSeq);
        setTemperature(1, HOTTEMP, timeSeq);
    break;
    case 3:
    Serial.println("Condition 3: hot(1) -- cold(2)");
    Serial.print("%");
           setTemperature(0, HOTTEMP, timeSeq);
         setTemperature(1, COLDTEMP, timeSeq);

    break;
    case 4:
    Serial.println("Condition 4: hot(1) -- hot(2)");
    Serial.print("%");
    setTemperature(0, HOTTEMP, timeSeq);
    setTemperature(1, HOTTEMP, timeSeq);
    break;
   case 99:
     if (spam){spam=false;}else {spam=true;}
    break;
    default:
    Serial.println("Wroing Choise");
    Serial.print("%");
    break;
  }
  Serial.print("%");
  Serial.println("%");
  
}

