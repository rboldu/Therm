// Here can be find the typical functions to control the peltier

void setStatus(int Peltier, int Status){
  peltier_Status[Peltier]=Status;
}

float getTemperature(int peltier){
  return  Temperature_peltier[peltier];
}



int getStatus(int peltier){
  return peltier_Status[peltier];
}


int getDuty(int peltier){
  return duty[peltier];
}


void setTemperature(int peltier, int temp, int time_ON){
  Ambienta[peltier]=0;
  wanted_temperature[peltier]=temp;
  peltier_Status[peltier]=ON;
  desiredTime[peltier]=time_ON;
  time_start_peltier[peltier]=millis();
  duty[peltier]=initDuty;
  logInitTemp[peltier]=getTemperature(peltier);
  logTime[peltier]=time_ON;
}


void setAmbient(int peltier, int temp, int time_ON){
  wanted_temperature[peltier]=temp;
  peltier_Status[peltier]=OFF;
  desiredTime[peltier]=time_ON;
  time_start_peltier[peltier]=millis();
  duty[peltier]=initDuty;
  Ambienta[peltier]=1;
}


void turnOffAllPeltier(){
  for (int i=0;i<NumPeltiers;i++){
    setStatus(i,OFF);
    Ambienta[i]=false;
  }
}


void incrieaseTemperature(int Peltier, int value,int temp){
  UpdateTemperature(Peltier);
  setTemperature(Peltier,getTemperature(Peltier)+value,temp);
  
}
//it prints the info of the peltier
void PeltierInfo(){
  Serial.println("---------------------INFO------------------");
  for (int i=0;i<NumPeltiers;i++){
      Serial.print("Peltier num:  ");
     Serial.print (i);
           Serial.print("  Status is:  ");
      Serial.print(getStatus(i));
    
 
    if (Cooling[i]==HOT){
    Serial.print("  HOT  ");
    }else{
      Serial.print("  COLD  ");
    }
     Serial.print("   Duty is:  ");
      Serial.print(getDuty(i));
      
    Serial.print("   Desired Temperature is:  ");
      Serial.print(wanted_temperature[i]);
    
    Serial.print("   Temperature is:  ");
      Serial.print(getTemperature(i));
      Serial.print("   Temperature BODY:  ");
      Serial.print(TemperatureBody);
     Serial.print("   Time missing is:  ");
     int timea=desiredTime[i]-(millis()-time_start_peltier[i]);
      Serial.println(timea);



    
  }

  Serial.println("----------------------------------------------------------------------");
}


int list[10][3]; //list of tasks
int listValue=0;
int listValuePointing=0;


// add a task on the list
void addWork(int Peltier,int temp,int timeON){
list[listValue][0]=Peltier;
list[listValue][1]=temp;
list[listValue][2]=timeON;
listValue++;
if (listValue==10) {listValue=0;}
}


//does the tasks, in case of no more task it will put Ambient temperature
void work(){
  int stat=0; // it knows if the peltiers are free.
    for (int i=0;i<NumPeltiers;i++){
      stat=getStatus(i)+stat;
  }
  
  if (listValuePointing!=listValue&&stat==0){
    
    setTemperature(list[listValuePointing][0], list[listValuePointing][1], list[listValuePointing][2]);
 
  listValuePointing++;
  if (listValuePointing==10) {listValuePointing=0;}
  
  }else{
    // On the Ambient temperature
    if (stat==0&&Ambient==true){
      for (int i=0;i<NumPeltiers;i++){
       Ambienta[i]=true;
       wanted_temperature[i]=AMBIENTTEMP;
      }
    }
  }
}
