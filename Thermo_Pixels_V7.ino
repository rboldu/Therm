//motor A connected between A01 and A02
//motor B connected between B01 and B02
/*
#define kp 7
#define ki 1
#define kd 0.9
#define dt 0.09
#define maxDuty 30
*/
#define FILTER 30
#define HOTTEMP 36
#define COLDTEMP 24
#define AMBIENTTEMP 30.5
#define kp 30 // the values are try and error. 
#define ki 15
#define kd 5
#define dt 0.09
#define maxDuty 200
#define initDuty 100
#define cold_duty_p 2.4
#define maxDutyCold 255
#define MAXDISTAMBIENT 8 // if the Temperature of the peltier is +-8 degrees from the "Ambient" it's going to wait. Avoid overcharge peltier.
#define RatioLog 10 // it will print the log 1 over 10 times

boolean spam=false; // spam, it prints status
boolean Ambient=true; //indicates if we want the peltiers to go to Ambient temperature
int tempBodySensor =A3; 
float TemperatureBody=0; // temperature of the body


int MAXTEMP=40; // maximum Temperature of the peltier. If it go over this Temperature it will turn off
const int NumPeltiers=2; //number of peltiers
long time_start_peltier[NumPeltiers]; //save when the peltier starts
int peltier_Status[NumPeltiers]; // save if the peltier is on/off
float Temperature_peltier[NumPeltiers]; // save the Temperature of the peltier
float wanted_temperature[NumPeltiers]; // it has the desired Temperature of the peltier
int peltier_pins[NumPeltiers][4]; // it define all the peltier pins, also the AN
int Motor[NumPeltiers]; 
long desiredTime[NumPeltiers]; // Time the peltier it's going to be on, if it's negative it means infinite.

int previusValue[NumPeltiers]; // PID
int duty[NumPeltiers]; // PWM of each peltier
int Ambienta[NumPeltiers]; // Indicate if we want Ambient Temperature
int Cooling[NumPeltiers]; // Indicates if the PWM it's positive or negative, basically if the peltier try to be warmer or color


float logInitTemp[NumPeltiers]; //Temperature of starting the process
float logEndTemp[NumPeltiers]; // temperature at the end
long logTime[NumPeltiers]; // time since starting



int ON=1;
int OFF=0;
int COLD=1;
int HOT=0;





void setup(){
  Serial.begin(250000);
  init_Peltier();
    
}


void loop(){

//setTemperature(0, 24, -1);
//setTemperature(0,38,-1);
//setAmbient(0,30,-1);
//setAmbient(1,30,-1);
/*wanted_temperature[0]=70;
peltier_Status[0]=ON;
peltier_Status[1]=ON;*/


 while (1){
  work();
  serialEvent();
    menu();

  for (int i=0; i<NumPeltiers;i++){
    
    MotorPeltier(i);

   if(spam){PeltierInfo();}
 }

 }
}





