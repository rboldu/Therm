
// private functions of the peltier

void peltier_pinsInit(){
  peltier_pins[0][0]=11; //PWM
  peltier_pins[0][1]=12; //Direction AIN1
  peltier_pins[0][2]=13; //Direction AIN2
  peltier_pins[0][3]=A1; //Sensor


  peltier_pins[1][0]=4; //PWM
  peltier_pins[1][1]=2; //Direction AIN1
  peltier_pins[1][2]=3; //Direction AIN2
  peltier_pins[1][3]=A2; //Sensor
  
}


void init_pinsDirection(int peltier){
    pinMode(peltier_pins[peltier][0],OUTPUT); //PWM
    pinMode(peltier_pins[peltier][1],OUTPUT); //Direction AIN1
    pinMode(peltier_pins[peltier][2],OUTPUT); //Direction AIN2
   
}



void UpdateTemperature(int i){
 // delay(10);
 float aux=0;
 float aux2=0;
 // Filter=average of few data. The sensors are a bit noisy
 for (int j=0;j<FILTER;j++){
  aux=calculTemp(i)+aux;
  aux2=calculTempBody()+aux2;
 }
 
  Temperature_peltier[i]=aux/FILTER;
  TemperatureBody=aux2/FILTER;
  
}

//Important it doesn't say that it's off, so it will stop the peltier but not write off
void stopPeltier(int i){
   analogWrite(peltier_pins[i][0],0);
    digitalWrite(peltier_pins[i][1], 0);
      digitalWrite(peltier_pins[i][2], 0);
  
}

//Checking max temperature
int NOTMaximumTemperature(int Peltier){
  return Temperature_peltier[Peltier]<MAXTEMP;
}


void PeltierController(int Peltier){
  
if (NOTMaximumTemperature(Peltier)){

    //Calculate the PID
    int error=wanted_temperature[Peltier]-Temperature_peltier[Peltier];  
    previusValue[Peltier]=(previusValue[Peltier]+error*dt);
    duty[Peltier]=(error*kp+previusValue[Peltier]*ki); // calculating duty based on pi algo

    
    if (duty[Peltier]>maxDuty) {duty[Peltier]=maxDuty;}  // check if the calculated duty values are above or below
    if (duty[Peltier]<-maxDutyCold){duty[Peltier]=-maxDutyCold;}  
    
    if (duty[Peltier]<0){
       Cooling[Peltier]=COLD;
       duty[Peltier]=duty[Peltier]*cold_duty_p; // The PID for cold needs a bit more of proporcional, as it's a bit more hard for the peltier.
       if (duty[Peltier]<-maxDuty){duty[Peltier]=-maxDutyCold;}
       digitalWrite(peltier_pins[Peltier][1],HIGH);
       digitalWrite(peltier_pins[Peltier][2],LOW);
    }else{
      
      if(duty[Peltier]>0){
         Cooling[Peltier]=HOT;
          digitalWrite(peltier_pins[Peltier][1],LOW);
          digitalWrite(peltier_pins[Peltier][2],HIGH);
          }      
    } 
    
// This case check if the peltier is trying to go to Ambient temperature. In the case of cold it can be saturated really fast
    if (Ambienta[Peltier]==1&&Cooling[Peltier]==COLD&&peltier_Status[Peltier]==OFF){
     duty[Peltier]=duty[Peltier]/(cold_duty_p); // reduce a bit the duty, in order to don't saturate the peltier.
      analogWrite(peltier_pins[Peltier][0],abs(duty[Peltier]));
    }else{
    analogWrite(peltier_pins[Peltier][0],abs(duty[Peltier]));
  }
  
  }else {
    // temperature overflow, turning off Peltier and Ambient temperature
    if (peltier_Status[Peltier]==OFF){ Ambienta[Peltier]=0;}
    PeltierInfo();
    Serial.println("Max Temperature");
    Serial.println("Turning off the Peltier");
    stopPeltier(Peltier);
    setStatus(Peltier, OFF); // It turn off the peltier
    
  }  
}




