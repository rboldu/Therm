

// init all the variables
void init_Peltier(){
  
  peltier_pinsInit();
  for (int i=0;i<NumPeltiers;i++){
    Cooling[i]=COLD;
    init_pinsDirection(i);
    time_start_peltier[i]=0;
    UpdateTemperature(i);//it updates the Temperature of the peltier
    //Temperature_peltier[i]=getTemperature(i); //TODO: At the begining I can't use this high level function, as the Temperature is not set at the begining
    wanted_temperature[i]=Temperature_peltier[i];
    peltier_Status[i]=OFF;
    desiredTime[i]=0;
    Motor[i]=0;
    previusValue[i]=0;
    duty[i]=initDuty;
    Ambienta[i]=0;
  }
}


void turnOffPeltier(int peltier){
  stopPeltier(peltier);
  peltier_Status[peltier]=OFF;
  Motor[peltier]=0;
  desiredTime[peltier]=0;
}

void MotorPeltier(int i){
  UpdateTemperature(i);
  switch (Motor[i]){
    case 0:
    
      if (peltier_Status[i]==ON){
        Motor[i]=1;       
      }else{
        turnOffPeltier(i);//it's not necessary but is not annoying
      }
      if (Ambienta[i]==1){PeltierController(i);}
    // delay(100);
      break;
    case 1:
    // It can be deleted
      Motor[i]=2;
      break;
      
    case 2:
      logPeltier(i);
      PeltierController(i); // it does the PID controller     
      
      if (desiredTime[i]<(millis()-time_start_peltier[i])&&desiredTime[i]>0){
        logEndTemp[i]=getTemperature(i);
       // It finish the task, we print the result
        Serial.print("&");
        Serial.print("Peltier num:   ");
        Serial.print (i);
        Serial.print("   Temperature init:  ");
        Serial.print(logInitTemp[i]);
        Serial.print("    Temp End:     ");
        Serial.print(logEndTemp[i]);
        Serial.print(".................");
        Serial.print(logTime[i]);
        Serial.println("S");
        
        Serial.println("Time out!!!");
        Serial.print("%");
        
        Motor[i]=3;
      }
      if (peltier_Status[i]==OFF){
        Motor[i]=3;
      }
      break; 
      
    case 3:
      turnOffPeltier(i);
      Motor[i]=0;    
      break;
  }
  
}

